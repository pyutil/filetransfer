# FILETRANSFER

## Maintainer: aachn3 <n45t31@protonmail.com>
## Site: <https://gitlab.com/pyutil/filetransfer>
## Version: 1.1.1

### About

This package aims to provide a uniform abstraction over file-like objects (directories, 
files and symlinks) accessible via different data transfer protocols. It utilizes the
[indexed class](https://gitlab.com/pyutil/indexed_class) interface to convert URLs such 
as _s3://bucket/file1_ or _file@text:///directory1_ to appropriate implementations of 
**FileTransfer**/**DirectoryTransfer** classes.

### Table of Contents

- [structure](#project-structure)
- [usage](#usage)
  - [examples](#code-samples)
- [support](#support)
  - [requirements](#prerequisites)
  - [installation](#installation)
- [tests](#testing-package-functionality)
  - [unit](#unit-tests)
  - [integration](#integration-tests)
  

### Project Structure

```
-core
  -exceptions
    -FileTransferBaseError(BaseException)
    -MetadataError(ValueError, FileTransferBaseError)
    -InvalidFileTypeError(ValueError, FileTransferBaseError)
    -FileTransferError(OSError, FileTransferBaseError)
    -DirectoryNotEmptyError(FileTransferError)
    -FileNotFoundError(FileTransferError, builtins.FileNotFoundError)
  -util
    -FileType(IntFlag)
      -FILE
      -DIRECTORY
      -LINK
    -parse_bool(data: any)
    -parse_dict(data: any)
    -makeurl(protocol: str, path: str, meta: dict, 
             mixin: optional([str, type(.mixin.FileType)]))
  -base
    -TransferBase(AbstractIndexedClass)
      -protocol (property)
      -url (property)
      -encoding (property)
      -set_default_filetype(type: [str, type(.mixin.FileType)]) (classmethod)
      -remove(ignore_errors: bool)
  -file
    -FileTransfer(.base.TransferBase, key=.util.FileType.FILE)
      -download()
      -upload(content: bytes)
      -path (property)
  -mixin
    -FileTypeMixin(AbstractIndexedClass)
    -TextMixin(FileTypeMixin, key="text")
      -download()
      -upload(content: str)
  -directory
    -DirectoryTransfer(.base.TransferBase, key=.util.FileType.DIRECTORY)
      -recursive (property)
      -ls(path_like: [str, iterable(str)], type_mask: optional(.util.FileType))
      -\_\_getitem\_\_(path: str)
      -\_\_iter\_\_()
      -empty (property)
      -remove(ignore_errors: bool)
  -link
    -LinkTransfer(.base.TransferBase, key=.util.FileType.LINK)
      -target (cached_property)
-file
  -OsFile(.core.file.FileTransfer, key="file")
    -chmod (property)
    -mkdir (property)
    -path (property)
    -download()
    -upload(content: bytes)
    -remove(ignore_errors: bool)
  -OsDirectory(.core.directory.DirectoryTransfer, key="file")
    -remove(ignore_errors: bool)
  -OsLink(.core.link.LinkTransfer, key="file")
    -remove(ignore_errors: bool)
-s3
  -S3Transfer(abc.ABC)
    -set_default_conn_args(**aws_conn_args) (classmethod)
    -aws_conn_args (property)
    -s3 (property)
    -bucket (property)
  -S3File(.S3Transfer, .core.file.FileTransfer, key="s3")
    -extra_args (property)
    -path (property)
    -download()
    -upload(content: bytes)
    -remove(ignore_errors: bool)
  -S3Directory(.S3Transfer, .core.directory.DirectoryTransfer, key="s3")
    -remove(ignore_errors: bool)
-http
  -RequestsTransfer(abc.ABC)
    -url (property)
    -method (property)
    -headers (property)
  -RequestsFile(.RequestsTransfer, .core.file.FileTransfer)
    -download()
    -upload(content: bytes)
    -remove(ignore_errors: bool)
  -HttpFile(.RequestsFile, key="http")
  -HttpsFile(.RequestsFile, key="https")
  -RequestsDirectory(.RequestsTransfer, .core.directory.DirectoryTransfer)
    -remove(ignore_errors: bool)
  -HttpDirectory(.RequestsDirectory, key="http")
  -HttpsDirectory(.RequestsDirectory, key="https") 
```

### Usage


#### Code samples

Copy file from HTTPS server to S3

```python3
from filetransfer import FileTransfer

with FileTransfer('https://domain.name/storage/file1').download() as content_stream:
    FileTransfer('s3://bucket/file1').upload(content_stream.getvalue())
```

Copy all JPEG files from one local directory to another

```python3
from filetransfer import FileTransfer, DirectoryTransfer, FileType

source, target = DirectoryTransfer('file://relative/directory1; recursive=1'), 'file:///absolute/directory2'
for file in source.ls(path_like='.*\.jpe?g$', type_mask=FileType.FILE):
    filename = file.path.split('/')[-1]
    backup = FileTransfer(f"{target}; filename={filename}")
    with file.download() as content:
        backup.upload(content.getvalue())
```

#### Support

##### Prerequisites
- python >= 3.8.0

##### Installation
`pip3 install filetransfer`

#### Testing package functionality

##### Unit tests

Format: None

##### Integration tests

Format: None
