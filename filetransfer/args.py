from argparse import ArgumentParser
from sys import argv

ap = ArgumentParser(
    "filetransfer", description="transfers data from @source to @target"
)
ap.add_argument("source", nargs=1, help="source file to download")
ap.add_argument(
    "target",
    nargs="?",
    default=".",
    help="target file to store downloaded content in; defaults to '.'",
)

PARSED_ARGS = ap.parse_args(argv[1:])


def parse_url(url: str):
    url = url.split("://")
    proto = "file" if len(url) == 1 else url[0]
    proto = proto.split("@")[0]
    return f"{proto}://{url[-1]}"
