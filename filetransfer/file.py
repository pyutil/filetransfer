from io import BytesIO
from os import chmod, listdir, makedirs, readlink, remove, unlink, walk
from os.path import dirname, exists, isdir, islink, join
from shutil import rmtree
from typing import Iterable, Optional, Union

from .core import (DirectoryTransfer, FileTransfer, FileType, LinkTransfer,
                   TransferBase)
from .exceptions import FileNotFoundError as _FileNotFoundError
from .exceptions import MetadataError
from .utils.parse import parse_bool


class _OsUtilMixin(TransferBase):
    def _typeof(self, path: str) -> FileType:
        if not exists(path):
            raise _FileNotFoundError(self.protocol, path)
        if isdir(path):
            return FileType.DIRECTORY
        if islink(path):
            return FileType.LINK
        return FileType.FILE

    def _follow(self, path: str) -> str:
        return readlink(path)


class OsFile(FileTransfer, key="file"):
    @property
    def chmod(self) -> Optional[int]:
        chmod = self.metadata.get("chmod")
        if chmod is None:
            return None
        if isinstance(chmod, int):
            return chmod
        if isinstance(chmod, str):
            try:
                return int(chmod, 8)
            except ValueError as e:
                raise MetadataError("chmod", chmod) from e
        raise MetadataError("chmod", chmod)

    @chmod.setter
    def chmod(self, value: Optional[Union[int, str]]):
        if isinstance(value, str):
            try:
                self.metadata["chmod"] = int(value, 8)
            except ValueError as e:
                raise MetadataError("chmod", value) from e
        elif value is None or isinstance(value, int):
            self.metadata["chmod"] = value
        else:
            raise MetadataError("chmod", value)

    @property
    def mkdir(self) -> bool:
        mkdir = self.metadata.get("mkdir")
        try:
            return parse_bool(mkdir)
        except KeyError as e:
            raise MetadataError("mkdir", mkdir) from e

    @mkdir.setter
    def mkdir(self, value: Union[int, str]):
        if not isinstance(value, (None.__class__, int, float, bool, str)):
            raise MetadataError("mkdir", value)
        try:
            self.metadata["mkdir"] = parse_bool(value)
        except KeyError as e:
            raise MetadataError("mkdir", value) from e

    @property
    def path(self) -> str:
        filename = self.metadata.get("filename")
        if filename is not None:
            return join(self.url, filename)
        return self.url

    def download(self) -> BytesIO:
        try:
            return BytesIO(open(self.path, "rb").read())
        except FileNotFoundError:
            raise _FileNotFoundError(self.proto, self.path)

    def upload(self, content: bytes):
        dirpath = dirname(self.path)
        if dirpath and self.mkdir:
            makedirs(dirpath, exist_ok=True)
        with open(self.path, "wb") as file:
            file.write(content)
        if self.chmod is not None:
            chmod(self.url, self.chmod)

    def remove(self, ignore_errors: bool = False) -> bool:
        try:
            remove(self.path)
            return True
        except FileNotFoundError:
            if not ignore_errors:
                raise _FileNotFoundError(self.proto, self.path)
        return False


class OsDirectory(_OsUtilMixin, DirectoryTransfer, key="file"):
    def _typeof(self, path: str) -> FileType:
        return super()._typeof(self._join(path))

    def _join(self, path: str) -> str:
        return join(self.url, path)

    def _ls(self) -> Iterable[str]:
        if self.recursive:
            for prefix, dirs, files in walk(self.url):
                prefix = prefix.replace(self.url, "")
                if prefix.startswith("/"):
                    prefix = prefix[1:]
                for dir in dirs:
                    yield join(prefix, dir)
                for file in files:
                    yield join(prefix, file)
        else:
            yield from listdir(self.url)

    def remove(self, ignore_errors: bool = False) -> bool:
        try:
            if super().remove(ignore_errors):
                rmtree(self.url)
                return True
        except FileNotFoundError:
            if not ignore_errors:
                raise _FileNotFoundError(self.proto, self.url)
        return False


class OsLink(_OsUtilMixin, LinkTransfer, key="file"):
    def remove(self, ignore_errors: bool = False) -> bool:
        try:
            unlink(self.url)
            return True
        except FileNotFoundError:
            if not ignore_errors:
                raise _FileNotFoundError(self.proto, self.url)
        return False
