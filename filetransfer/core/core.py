from abc import abstractmethod
from enum import IntFlag
from typing import Optional, Type, Union

from indexed_class import AbstractIndexedClass, NoMatchingSubclassError

from ..exceptions import InvalidFileTypeError, InvalidMixinError
from ..utils.mixin import FileTypeMixin
from .uri import Uri


class FileType(IntFlag):
    FILE = 1
    DIRECTORY = 2
    LINK = 4


class TransferBase(AbstractIndexedClass, root=True):
    @classmethod
    def _create(cls, mixin):
        instance = super().__new__(cls)
        instance.__mixin__ = mixin
        return instance

    def __new__(cls, uri: Union[str, Uri] = None):
        if uri is None:
            return super().__new__(cls)

        if isinstance(uri, str):
            uri = Uri.parse(uri)

        try:
            cls, type_mixin = cls[uri.protocol], None
        except NoMatchingSubclassError as e:
            raise InvalidFileTypeError(uri.protocol) from e

        if cls.__MIXIN__:
            type_mixin = cls.__MIXIN__
        if uri.mixin:
            try:
                type_mixin = FileTypeMixin[uri.mixin]
            except NoMatchingSubclassError as e:
                raise InvalidMixinError(uri.mixin) from e

        instance = cls._create(type_mixin)
        instance.uri = uri

        return instance

    @property
    def protocol(self) -> str:
        return self.__class__.key

    @property
    def url(self) -> str:
        return self.uri.path

    @property
    def metadata(self) -> dict:
        return self.uri.metadata

    @property
    def encoding(self) -> str:
        return self.metadata.get("encoding", "utf-8")

    @encoding.setter
    def encoding(self, encoding: str):
        self.metadata["encoding"] = encoding

    @classmethod
    def set_default_filetype(
        cls, type: Optional[Union[str, Type[FileTypeMixin]]] = None
    ):
        if isinstance(type, str):
            try:
                type = FileTypeMixin[type]
            except NoMatchingSubclassError as e:
                raise InvalidMixinError(type) from e
        cls.__MIXIN__ = type

    @abstractmethod
    def remove(self, ignore_errors: bool = False) -> bool:
        raise NotImplementedError("abstract method requires override")

    def __str__(self) -> str:
        return str(self.uri)
