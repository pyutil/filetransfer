from abc import abstractmethod
from re import compile
from typing import Any, Iterable, Optional, Tuple, Union

from ..config import config
from ..exceptions import DirectoryNotEmptyError, MetadataError
from ..utils.caching import PathCache
from ..utils.parse import parse_bool
from .core import FileType, TransferBase
from .file import FileTransfer
from .link import LinkTransfer
from .uri import Uri


class DirectoryTransfer(TransferBase, root=True):
    __MIXIN__ = None

    def __init__(self, *args, **kwargs):
        self.recursive = self.metadata.get("recursive")
        # config.monitor(self, "DIRECTORY_PATH_CACHING")

    def notify(self, value: str, old: Any, new: Any):
        if value == "DIRECTORY_PATH_CACHING":
            if new and not old:
                self._path_cache = PathCache[getattr(self, "PATH_WRAPPER", None)]()
                self._typeof = self._path_cache.type_source(self._typeof)
            if old and not new:
                self._typeof = self._typeof.__fun__
                del self._path_cache

    @property
    def recursive(self) -> bool:
        recursive = self.metadata.get("recursive")
        try:
            return parse_bool(recursive)
        except KeyError as e:
            raise MetadataError("recursive", recursive) from e

    @recursive.setter
    def recursive(self, value: Union[str, bool]):
        try:
            self.metadata["recursive"] = parse_bool(value)
        except KeyError as e:
            raise MetadataError("recursive", value) from e

    def _join(self, path: str) -> str:
        return f"{self.url}/{path}"

    def _makeurl(self, path: str) -> str:
        return str(Uri(self.protocol, self.__mixin__, self._join(path), self.metadata))

    @abstractmethod
    def _typeof(self, path: str) -> FileType:
        raise NotImplementedError("abstract method requires override")

    @abstractmethod
    def _ls(self) -> Iterable[str]:
        raise NotImplementedError("abstract method requires override")

    def ls(
        self,
        *,
        path_like: Optional[Union[str, Iterable[str]]] = None,
        type_mask: Optional[FileType] = None,
    ) -> Iterable[TransferBase]:
        filters = []

        if type_mask:
            filters.append(lambda _, type: type & type_mask)

        if isinstance(path_like, str):
            path_like = [compile(path_like)]
        elif path_like:
            path_like = [compile(pattern) for pattern in path_like]
        if path_like:
            filters.append(
                lambda path, _: any(pattern.match(path) for pattern in path_like)
            )

        for path in self._ls():
            type = self._typeof(path)
            for filter in filters:
                if not filter(path, type):
                    break
            else:
                yield TransferBase[type](self._makeurl(path))

    def directory(self, path: str):
        return DirectoryTransfer(self._makeurl(path))

    def file(self, path: str):
        return FileTransfer(self._makeurl(path))

    def link(self, path: str):
        return LinkTransfer(self._makeurl(path))

    def __getitem__(self, path: Union[str, Tuple[FileType, str]]):
        if isinstance(path, str):
            type = self._typeof(path)
        else:
            type, path = path
        return TransferBase[type](self._makeurl(path))

    def __iter__(self):
        return self.ls()

    @property
    def empty(self) -> bool:
        try:
            return not next(self.ls())
        except StopIteration:
            return True

    def remove(self, ignore_errors: bool = False) -> bool:
        if not self.empty and not self.recursive:
            if not ignore_errors:
                raise DirectoryNotEmptyError(self.protocol, self.url)
            return False
        return True


TransferBase[FileType.DIRECTORY] = DirectoryTransfer
