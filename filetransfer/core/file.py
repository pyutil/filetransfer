from abc import abstractmethod
from io import BytesIO
from typing import Optional, Type

from indexed_class import IndexedClass

from ..utils.mixin import FileTypeMixin
from .core import FileType, TransferBase


class FileTransfer(TransferBase, root=True):
    __MIXIN__ = None

    @classmethod
    def _create(cls, mixin: Optional[Type[FileTypeMixin]]):
        return super().__new__(
            cls
            if mixin is None
            else type(IndexedClass)(
                f"{cls.__name__}{mixin.__name__}",
                (mixin, cls),
                {
                    "__registry__": cls.__registry__,
                    "file_type": property(lambda self: mixin.key),
                },
            )
        )

    @abstractmethod
    def download(self) -> BytesIO:
        raise NotImplementedError("abstract method requires override")

    @abstractmethod
    def upload(self, content: bytes):
        raise NotImplementedError("abstract method requires override")

    @property
    def path(self) -> str:
        filename = self.metadata.get("filename")
        if filename is not None:
            return f"{self.url}/{self.filename}"
        return self.url


TransferBase[FileType.FILE] = FileTransfer
