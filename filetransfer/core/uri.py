from ast import literal_eval
from importlib import import_module
from io import StringIO
from re import compile
from typing import Any, Dict, Optional, Union

from ..config import config
from ..exceptions import MalformedUriError


class ExternalEntity:
    class _default_package(dict):
        def __init__(self):
            super().__init__(globals())

        def __getattr__(self, attrname: str):
            return self[attrname]

    _pattern = compile(
        "^(?P<spec><"
        "(?:(?P<package>[A-Za-z_][A-Za-z0-9_]*)::)?"
        "(?P<modpath>"
        "(?:[A-Za-z_][A-Za-z0-9_]*\\.)*"
        "(?:[A-Za-z_][A-Za-z0-9_]*)"
        ")"
        ">)"
        "(?:#\s*(?P<params>.*))?$"
    )

    def __init__(self, uri: str):
        match = self._pattern.match(uri)
        if not match:
            raise MalformedUriError(
                uri, "external spec must adhere to '(package::)module.path' pattern"
            )
        if not config.ALLOW_EXTERNAL:
            raise ExternalNotAllowedError()
        self.spec = match["spec"]
        self.params = ExternalParams(match["params"])
        self.package = match["package"] or None
        self.modpath = match["modpath"]

    @property
    def loader(self) -> Any:
        package = (
            import_module(self.package) if self.package else self._default_package()
        )
        loader = package
        for path_node in self.modpath.split("."):
            loader = getattr(loader, path_node)
        return loader

    def __str__(self) -> str:
        return self.spec


class Url:
    def __init__(self, url: str):
        partition = url.split(";")
        self.path = partition[0]
        self.metadata = {}
        for entry in partition[1:]:
            key, value = entry.split("=")
            # if config.EVAL_META:
            # value = self.parse_node(literal_eval(value))
            # else:
            # value = value.strip()
            value = value.strip()
            self.metadata[key.strip()] = value

    @staticmethod
    def parse_node(node: Any):
        if isinstance(node, dict):
            return {key: Url.parse_node(value) for key, value in node.items()}
        if isinstance(node, list):
            return [Url.parse_node(value) for value in node]
        if isinstance(node, str):
            try:
                return ExternalEntity(node)
            except MalformedUriError:
                return node
        return node


class ExternalParams:
    def __init__(self, params: Union[str, Dict[str, Any]] = None):
        if params:
            if isinstance(params, str):
                params = Url.parse_node(literal_eval(params))
            self.args = params.pop("args", [])
            self.kwargs = params
        else:
            self.args, self.kwargs = [], {}


class Uri:
    _pattern = compile(
        "(?P<protocol>[A-Za-z0-9_<>]+)"
        "(?:@(?P<mixin>[A-Za-z0-9_]+))?"
        "://"
        "(?P<url>.*)"
    )

    @classmethod
    def parse(cls, uri: str):
        match = cls._pattern.match(uri)
        if not match:
            raise MalformedUriError(
                uri, "filetransfer uri must adhere to 'protocol(@mixin)://url' pattern"
            )
        mixin = match["mixin"] or None
        url = Url(match["url"])
        path, meta = url.path, url.meta
        try:
            # TODO: add external entity enabler/disabler
            protocol = ExternalEntity(match["protocol"])
            params = meta.pop("PARAMS", None)
            if params:
                protocol.params = ExternalParams(params)
        except MalformedUriError:
            protocol = match["protocol"]
        return Uri(protocol, mixin, path, meta)

    def __init__(
        self,
        protocol: Union[str, ExternalEntity],
        mixin: Optional[str],
        path: str,
        meta: Optional[dict] = {},
    ):
        self.protocol = protocol
        self.mixin = mixin
        self.path = path
        self.metadata = meta

    def __str__(self) -> str:
        with StringIO() as out:
            out.write(str(self.protocol))
            if self.mixin:
                out.write("@")
                out.write(mixin if isinstance(mixin, str) else mixin.key)
            out.write("://")
            out.write(self.path)
            for key, value in self.metadata.items():
                out.write(";")
                out.write(key)
                out.write("=")
                out.write(str(value))
            return out.getvalue()
