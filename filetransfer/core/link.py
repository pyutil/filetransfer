from abc import abstractmethod
from functools import cached_property

from .core import FileType, TransferBase
from .uri import Uri


class LinkTransfer(TransferBase, root=True):
    __MIXIN__ = None

    def _makeurl(self, path: str):
        return str(Uri(self.protocol, self.__mixin__, path, self.metadata))

    @abstractmethod
    def _follow(self, path: str) -> str:
        raise NotImplementedError("abstract method requires override")

    @abstractmethod
    def _typeof(self, path: str) -> FileType:
        raise NotImplementedError("abstract method requires override")

    @cached_property
    def target(self) -> TransferBase:
        stack = set()
        path, type = self.url, FileType.LINK
        while True:
            if path in stack:
                return self
            stack.add(path)
            path = self._follow(path)
            type = self._typeof(path)
            if type != FileType.LINK:
                break
        return TransferBase[type](self._makeurl(path))


TransferBase[FileType.LINK] = LinkTransfer
