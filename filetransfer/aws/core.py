from abc import ABC
from typing import Any, Dict, Optional

from boto3 import client, resource
from boto3.resources.base import ServiceResource
from botocore.client import BaseClient
from botocore.exceptions import ClientError

from .exceptions import AwsApiBaseError


class AwsErrorWrapper:
    def __init__(self, client: Any):
        self.__client__ = client

    def __getattr__(self, attrname: str):
        if attrname == "__client__":
            return object.__getattr__(self, attrname)
        attr = getattr(self.__client__, attrname)
        if callable(attr):

            def wrapper(*args, **kwargs):
                print(f"accessing wrapped {attrname}")
                try:
                    result = attr(*args, **kwargs)
                except AwsApiBaseError:
                    raise
                except ClientError as e:
                    raise AwsApiBaseError(e.response, e.operation_name) from e
                return (
                    AwsErrorWrapper(result)
                    if isinstance(result, (ServiceResource, BaseClient))
                    else result
                )

            return wrapper
        return attr


class AwsTransfer(ABC):
    AWS_CONN_ARGS: Dict[str, Any] = {}

    @classmethod
    def set_default_conn_args(cls, **aws_conn_args: Dict[str, Any]):
        cls.AWS_CONN_ARGS = aws_conn_args

    @property
    def aws_conn_args(self) -> Dict[str, Any]:
        return self.AWS_CONN_ARGS

    @aws_conn_args.setter
    def aws_conn_args(self, aws_conn_args: Optional[Dict[str, Any]]):
        if aws_conn_args is None:
            try:
                del self.AWS_CONN_ARGS
            except AttributeError:
                pass
        else:
            self.AWS_CONN_ARGS = aws_conn_args

    @property
    def client(self):
        return AwsErrorWrapper(client(self.__class__.key, **self.aws_conn_args))

    @property
    def resource(self):
        return AwsErrorWrapper(resource(self.__class__.key, **self.aws_conn_args))
