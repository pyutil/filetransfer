import builtins
from functools import cached_property
from typing import Any, Dict

from botocore.exceptions import ClientError
from indexed_class import IndexedClass

from ..exceptions import FileTransferBaseError


class AwsApiBaseError(ClientError, FileTransferBaseError):
    def __new__(
        cls,
        error_response: Dict[str, Any],
        operation_name: str,
    ):
        code = int(error_response.get("ResponseMetadata", {}).get("HTTPStatusCode", -1))
        try:
            cls = AwsApiError[code]
        except KeyError:
            pass
        return super().__new__(cls, error_response, operation_name)

    def __init__(self, error_response: Dict[str, Any], operation_name: str):
        super().__init__(error_response, operation_name)
        self.details = error_response.get("Error", {}).get("Message", "")
        self.type = error_response.get("Error", {}).get("Code", self.__class__.__name__)

    @cached_property
    def code(self) -> int:
        return int(self.response.get("ResponseMetadata", {}).get("HTTPStatusCode", -1))


class AwsBucketRemovalError(AwsApiBaseError):
    def __init__(self, error_response, operation):
        super().__init__(error_response, operation)
        self.bucket = error_response.get("Error", {}).get("BucketName", "UNSPECIFIED")


class AwsApiError(AwsApiBaseError, IndexedClass, root=True):
    @cached_property
    def code(self) -> int:
        return self.__class__.key


class AwsRequestError(AwsApiError, key=400):
    pass


class AwsAuthError(AwsApiError, key=403):
    pass


class AwsFileNotFoundError(AwsApiError, builtins.FileNotFoundError, key=404):
    pass
