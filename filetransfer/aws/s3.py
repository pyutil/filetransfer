import json
from io import BytesIO
from json.decoder import JSONDecodeError
from typing import Optional, Union

from ..core import DirectoryTransfer, FileTransfer, FileType
from ..exceptions import MetadataError
from ..utils.caching import BoolablePosixPath
from .core import AwsTransfer
from .exceptions import AwsBucketRemovalError, AwsFileNotFoundError


class S3Transfer(AwsTransfer):
    PATH_WRAPPER = BoolablePosixPath

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        try:
            self.bucket, self.key = self.url.split("/", 1)
            self.key = self.PATH_WRAPPER(self.key)
        except ValueError:
            self.bucket, self.key = self.url, self.PATH_WRAPPER()

    @property
    def bucket(self):
        return self.resource.Bucket(self._bucket)

    @bucket.setter
    def bucket(self, value: str):
        self._bucket = value


class S3File(S3Transfer, FileTransfer, key="s3"):
    @property
    def extra_args(self):
        args = self.metadata.get("extra-args", {})
        if args is None:
            return {}
        if isinstance(args, dict):
            return args
        if isinstance(args, str):
            try:
                return json.loads(args)
            except JSONDecodeError:
                pass
        raise MetadataError("extra-args", args)

    @extra_args.setter
    def extra_args(self, value: Optional[Union[dict, str]]):
        if value is None or isinstance(value, dict):
            self.metadata["extra-args"] = value or {}
        elif isinstance(value, str):
            try:
                self.metadata["extra-args"] = json.loads(value)
            except JSONDecodeError as e:
                raise MetadataError("extra-args", value) from e
        else:
            raise MetadataError("extra-args", value)

    @property
    def path(self) -> str:
        filename = self.metadata.get("filename")
        if filename is not None:
            out = self.key / filename
        else:
            out = self.key or ""
        return str(out)

    def download(self) -> BytesIO:
        bi = BytesIO()
        self.bucket.download_fileobj(self.path, bi)
        bi.seek(0)
        return bi

    def upload(self, content: bytes):
        with BytesIO(content) as bi:
            self.bucket.upload_fileobj(
                bi,
                self.path,
                ExtraArgs=self.extra_args,
            )

    def remove(self, ignore_errors: bool = False) -> bool:
        file = self.bucket.Object(self.path)
        file.load()
        file.delete()
        return True


class S3Directory(S3Transfer, DirectoryTransfer, key="s3"):
    ALLOW_BUCKET_REMOVAL = False

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def _typeof(self, path: str) -> FileType:
        try:
            meta = self.client.head_object(
                Bucket=self.bucket.name,
                Key=str(self.key / path),
            )
            return FileType.FILE if meta.get("ContentLength", 0) else FileType.LINK
        except AwsFileNotFoundError:
            meta = self.client.head_object(
                Bucket=self.bucket.name,
                Key=f"{self.key / path}/",
            )
            return FileType.DIRECTORY

    def _ls(self):
        params = {"Bucket": self.bucket.name}
        if self.key:
            params["Prefix"] = f"{self.key}/"
        for file in self.client.list_objects(**params).get("Contents", []):
            path = self.PATH_WRAPPER(file["Key"]).relative_to(self.key)
            if not path:
                continue
            if len(path.parts) > 1 and not self.recursive:
                continue
            yield path

    def remove(self, ignore_errors: bool = False) -> bool:
        if super().remove(ignore_errors):
            bucket = self.bucket
            if not self.key:
                if not self.ALLOW_BUCKET_REMOVAL:
                    raise AwsBucketRemovalError({}, "RemoveBucket")
                for file in bucket.objects.all():
                    file.delete()
                bucket.delete()
            else:
                files = list(bucket.objects.filter(Prefix=f"{self.key}/"))
                if not files:
                    raise AwsFileNotFoundError(
                        {},
                        "ListFiles",
                    )
                for file in files:
                    file.delete()
                self.bucket.Object(self.key).delete()
            return True
        return False
