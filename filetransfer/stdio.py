from io import BytesIO, UnsupportedOperation
from sys import stderr, stdin, stdout

from .core import FileTransfer
from .exceptions import (FileNotFoundError, FileNotReadableError,
                         FileNotWritableError)


class StdIOFileTransfer(FileTransfer, key="stdio"):
    class IOReadError(FileNotReadableError, UnsupportedOperation):
        pass

    class IOWriteError(FileNotWritableError, UnsupportedOperation):
        pass

    def __init__(self, *args, **kwargs):
        if self.url == "stdin":
            self.stream = stdin
        elif self.url == "stdout":
            self.stream = stdout
        elif self.url == "stderr":
            self.stream = stderr
        else:
            raise FileNotFoundError(self.protocol, self.url)

    def download(self) -> BytesIO:
        try:
            return BytesIO(self.stream.read().encode(self.encoding))
        except UnsupportedOperation:
            raise self.IOReadError(self.protocol, self.stream)

    def upload(self, data: bytes):
        try:
            self.stream.write(data.decode(self.encoding))
        except UnsupportedOperation:
            raise self.IOWriteError(self.protocol, self.stream)

    def remove(self):
        if not self.stream.closed():
            self.stream.close()
