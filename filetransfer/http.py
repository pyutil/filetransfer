from abc import ABC
from io import BytesIO
from json import loads
from json.decoder import JSONDecodeError
from typing import Iterable, Union

from .core import DirectoryTransfer, FileTransfer, FileType
from .exceptions import MetadataError

try:
    from requests import request

    class RequestsTransfer(ABC):
        def __init__(self, *args, **kwargs):
            self.headers = self.metadata.get("headers", {})

        @property
        def url(self) -> str:
            return f"{self.__class__.key}://{self._url}"

        @property
        def method(self) -> str:
            method = self.metadata.get("method", "GET")
            try:
                return method.upper()
            except AttributeError as e:
                raise MetadataError("method", method) from e

        @method.setter
        def method(self, value: str):
            try:
                self.metadata["method"] = value.upper()
            except AttributeError as e:
                raise MetadataError("method", value) from e

        @property
        def headers(self) -> dict:
            headers = self.metadata.get("headers", {})
            if headers is None:
                return {}
            if isinstance(headers, {}):
                return headers
            if isinstance(headers, str):
                try:
                    return loads(headers)
                except JSONDecodeError as e:
                    raise MetadataError("headers", headers) from e
            raise MetadataError("headers", headers)

        @headers.setter
        def headers(self, value: Union[str, dict]):
            if isinstance(value, str):
                try:
                    self.metadata["headers"] = loads(value)
                except JSONDecodeError as e:
                    raise MetadataError("headers", value)
            elif value is None or isinstance(value, dict):
                self.metadata["headers"] = value
            else:
                raise MetadataError("headers", value)

    class RequestsFile(RequestsTransfer, FileTransfer):
        def download(self) -> BytesIO:
            response = request(
                self.method,
                self.url,
                headers=self.headers,
            )
            return BytesIO(response.content)

        def upload(self, content: bytes):
            with BytesIO(content) as bi:
                request(
                    self.method,
                    self.url,
                    files={
                        "upload_file": (
                            self.metadata.get("filename"),
                            bi,
                            self.metadata.get("content-type", "text/plain"),
                        )
                    },
                    headers=self.headers,
                )

        def remove(self, ignore_errors: bool = False) -> bool:
            try:
                return super().remove(ignore_errors)
            except Exception:
                if not ignore_errors:
                    raise
            return False

    class HttpFile(RequestsFile, key="http"):
        pass

    class HttpsFile(RequestsFile, key="https"):
        pass

    try:
        from bs4 import BeautifulSoup

        class RequestsDirectory(RequestsTransfer, DirectoryTransfer):
            def _typeof(self, path: str) -> int:
                return FileType.DIRECTORY if path.endswith("/") else FileType.FILE

            def _ls(self) -> Iterable[str]:
                response = request(self, self.method, self.url, headers=self.headers)
                for link in BeautifulSoup(response.text, "html.parser").find_all("a"):
                    link = link.get("href")
                    if "://" in link:
                        continue
                    yield link

            def remove(self, ignore_errors: bool = False) -> bool:
                try:
                    return super().remove(ignore_errors)
                except Exception:
                    if not ignore_errors:
                        raise
                return False

        class HttpDirectory(RequestsDirectory, key="http"):
            pass

        class HttpsDirectory(RequestsDirectory, key="https:"):
            pass

    except ImportError:
        pass
except ImportError:
    pass
