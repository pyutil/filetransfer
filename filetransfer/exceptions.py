import builtins
from typing import Any

from indexed_class import NoMatchingSubclassError


class FileTransferBaseError(BaseException):
    pass


# CORE ERRORS
class MalformedUriError(ValueError, FileTransferBaseError):
    def __init__(self, uri: str, details: str = "UNKNOWN ERROR"):
        if len(uri) > 50:
            uri = f"{uri[:50]}..."
        super().__init__(f"invalid uri: '{uri}' ({details})")


class MetadataError(ValueError, FileTransferBaseError):
    def __init__(self, field: str, value: Any):
        super().__init__(f"invalid {field.upper()} value: {value}")


class InvalidFileTypeError(NoMatchingSubclassError, FileTransferBaseError):
    def __init__(self, type: Any):
        super().__init__(f"requested invalid file type: {type}")


class InvalidMixinError(NoMatchingSubclassError, FileTransferBaseError):
    def __init__(self, type: Any):
        super().__init__(f"requested invalid file mixin: {type}")


# TRANSFER ERRORS
class FileTransferError(OSError, FileTransferBaseError):
    def __init__(self, proto: str, url: str, *args):
        super().__init__(self.MSG_TEMPLATE.format(proto.upper(), url, *args))


class DirectoryNotEmptyError(FileTransferError):
    MSG_TEMPLATE = "{} directory '{}' not empty"


class FileNotFoundError(FileTransferError, builtins.FileNotFoundError):
    MSG_TEMPLATE = "{} file '{}' not found"


class FileNotWritableError(FileTransferError):
    MSG_TEMPLATE = "{} file '{}' not writable"


class FileNotReadableError(FileTransferError):
    MSG_TEMPLATE = "{} file '{}' not readable"
