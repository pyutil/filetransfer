from json import loads
from json.decoder import JSONDecodeError
from typing import Any

_STR_BOOL = {
    **{str: True for str in ("1", "y", "yes", "true")},
    **{str: False for str in ("0", "n", "no", "false")},
}


def parse_bool(data: Any) -> bool:
    if isinstance(data, str):
        return _STR_BOOL[data.lower()]
    return bool(data)


def parse_dict(data: Any) -> dict:
    if isinstance(data, (dict, None.__class__)):
        return data or {}
    if isinstance(data, (str, bytes)):
        try:
            return loads(data)
        except JSONDecodeError as e:
            raise ValueError(*e.args) from e
    return dict(data)
