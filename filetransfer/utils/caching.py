from functools import cached_property
from io import StringIO
from pathlib import PurePath, PurePosixPath
from typing import (Callable, Generic, Iterable, Optional, Tuple, Type,
                    TypeVar, Union)

from ..core import FileType


class BoolablePosixPath(PurePosixPath):
    def __bool__(self) -> bool:
        return len(self.parts) > 0


path_wrapper = TypeVar("path_wrapper", bound=Optional[PurePath])


class PathCache(Generic[path_wrapper]):
    PATH_WRAPPER = PurePosixPath

    @cached_property
    def wrapper(self):
        try:
            wrapper = self.__orig_class__.__args__[0]
            if not wrapper or wrapper is None.__class__:
                raise ValueError()
            return wrapper
        except (ValueError, AttributeError, IndexError):
            return self.PATH_WRAPPER

    def wrap(self, path: Union[path_wrapper, str]) -> Optional[path_wrapper]:
        if path is None:
            return self.wrapper()
        if isinstance(path, str):
            return self.wrapper(path)
        if isinstance(path, self.wrapper):
            return path
        raise ValueError(f"invalid path specification: {path}")

    class PathTreeNode(Generic[TypeVar("owner")]):
        @cached_property
        def owner(self) -> Optional["PathCache"]:
            try:
                return self.__orig_class__.__args__[0]
            except (AttributeError, IndexError):
                return None

        def wrap(self, path: Union[path_wrapper, str]) -> path_wrapper:
            return getattr(self.owner, "wrap", lambda arg: arg)(path)

        def __init__(self, prefix: path_wrapper = None, type: FileType = None):
            self.subpaths = {}
            self.prefix = prefix
            self.type = type

        def split(
            self, path: Union[path_wrapper, str]
        ) -> Tuple[str, path_wrapper, path_wrapper]:
            path, prefix = self.wrap(path), self.wrap(self.prefix)
            if prefix.parts:
                path = path.relative_to(prefix)
            if path.parts:
                return path.parts[0], prefix / path.parts[0], path
            return None, prefix, None

        def __getitem__(self, path: Union[path_wrapper, str]) -> "PathTreeNode":
            try:
                subpath, _, remainder = self.split(path)
            except ValueError as e:
                raise KeyError(f"not a path relative to {self.prefix}") from e
            if not remainder:
                return self
            return self.subpaths[subpath][path]

        def set(self, path: Union[path_wrapper, str], type: FileType) -> "PathTreeNode":
            subpath, prefix, remainder = self.split(path)
            if not remainder:
                if not self.subpaths:
                    self.FileType = type
            else:
                self.subpaths[subpath] = node = self.subpaths.get(
                    subpath,
                    self.__class__(prefix, type),
                )
                node[path] = type
                self.type = FileType.DIRECTORY
            return self

        def __setitem__(self, path: Union[path_wrapper, str], type: FileType):
            self.set(path, type)

        def items(self) -> Iterable[Tuple[path_wrapper, "PathTreeNode"]]:
            output = {}
            _, prefix, __ = self.split(self.prefix)
            if prefix.parts and self.type is not None:
                output[prefix] = self
            for subpath in self.subpaths.values():
                output.update(subpath.items())
            return output.items()

    def __init__(self):
        self._tree, self._flat = self.PathTreeNode[self](), {}

    def __getitem__(self, path: Union[path_wrapper, str]) -> FileType:
        return self._flat[self.wrap(path)].type

    def __setitem__(self, path: Union[path_wrapper, str], type: FileType):
        path = self.wrap(path)
        if path not in self._flat:
            node = self._tree.set(path, type)
            self._flat.update(node.items())

    def type_source(
        self, fun: Callable[[Union[path_wrapper, str]], FileType]
    ) -> Callable[[Union[path_wrapper, str]], FileType]:
        def wrapper(path: str):
            try:
                return self[path]
            except KeyError:
                self[path] = type = fun(path)
                return type

        wrapper.__fun__ = fun
        return wrapper
