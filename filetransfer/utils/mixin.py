from io import StringIO

from indexed_class import AbstractIndexedClass


class FileTypeMixin(AbstractIndexedClass, root=True):
    pass


class TextMixin(FileTypeMixin, key="text"):
    def download(self) -> StringIO:
        with super().download() as source:
            return StringIO(source.read().decode(self.encoding))

    def upload(self, content: str):
        super().upload(content.encode(self.encoding))
