from dataclasses import dataclass


@dataclass
class FileTransferConfig:
    DIRECTORY_PATH_CACHING: bool = True
    WRAP_EXCEPTIONS: bool = True
    FILE_EXTENSIONS: bool = False

    METADATA_STRICT: bool = False
    METADATA_EVAL: bool = False


config = FileTransferConfig()
