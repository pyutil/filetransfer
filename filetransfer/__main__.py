from os.path import sep

from . import FileTransfer


def filetransfer():
    from .args import PARSED_ARGS as arguments
    from .args import parse_url

    source, target = arguments.source[0], arguments.target

    source = FileTransfer(parse_url(source))
    if target == ".":
        if source.protocol == "file":
            target = source.path.split(sep)[-1]
        else:
            target = source.path.split("/")[-1]
    target = FileTransfer(parse_url(target))

    with source.download() as content:
        target.upload(content.getvalue())


if __name__ == "__main__":
    filetransfer()
